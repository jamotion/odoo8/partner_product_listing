# -*- coding: utf-8 -*-
{
    'author': 'Jamotion GmbH',
    'category': 'Website',
    'data': [
        'views/res_partner.xml',
        'views/product_public_category.xml'
    ],
    'depends': [
        'website_sale_product_characteristics',
        'triopan_faltsignal_wizard',
    ],
    'installable': True,
    'license': 'AGPL-3',
    'name': 'Partner / Product Listing',
    'summary': 'Restrict Access to some Public Product Categories',
    'version': '8.0.1.0.0',
    'website': 'https://jamotion.ch',
}
