# -*- coding: utf-8 -*-
import logging
from openerp import models, fields, api, _

_logger = logging.getLogger(__name__)


class ResPartner(models.Model):
    _inherit = 'res.partner'

    public_category_ids = fields.Many2many(
            comodel_name='product.public.category',
            relation='partner_listing',
            column1='partner_id',
            column2='category_id',
            string='Listings',
            domain=[('customer_listing', '=', True)],
    )

    excluded_public_category_ids = fields.Many2many(
            comodel_name='product.public.category',
            relation='excluded_partner_listing',
            column1='partner_id',
            column2='category_id',
            string='Excluded Listings',
            domain=[('customer_listing', '=', False)],
    )