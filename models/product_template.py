# -*- coding: utf-8 -*-
import logging

from openerp.http import request
from openerp import api, models, fields

_logger = logging.getLogger(__name__)

class ProductTemplate(models.Model):
    _inherit = 'product.template'

    excluded_by_listing = fields.Boolean(
            string='Excluded by Listing',
            compute='_compute_excluded_by_listing',
    )

    @api.multi
    def check_is_excluded_by_listing(self):
        self.ensure_one()
        return self.sudo().excluded_by_listing

    @api.multi
    def _compute_excluded_by_listing(self):
        _logger.warning('Compute Excluded Listings')
        public_group = self.env.ref('base.group_public')
        portal_group = self.env.ref('base.group_portal')
        if public_group in request.env.user.groups_id or portal_group in request.env.user.groups_id:
            _logger.warning('Public or Portal group, check excluded listings')
            excluded_listing_cat_ids = request.env.user.partner_id.commercial_partner_id.excluded_public_category_ids.ids
            excluded_listing_cat_ids += request.env.user.partner_id.excluded_public_category_ids.ids

            for record in self.filtered(lambda f: f.public_categ_ids):
                if any(pc in excluded_listing_cat_ids for pc in record.public_categ_ids.ids):
                    record.excluded_by_listing = True

        else:
            _logger.warning('Internal Group, ignore excluded listings')

