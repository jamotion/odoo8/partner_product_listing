# -*- coding: utf-8 -*-
import logging
from openerp import models, fields, api, _
from openerp.http import request

_logger = logging.getLogger(__name__)


class ProductPublicCategory(models.Model):
    _inherit = 'product.public.category'

    customer_listing = fields.Boolean(
            string='Customer Listing',
            help='When active, only customers having relation to this category can see these products',
            index=True,
    )

    partner_listing_ids = fields.Many2many(
            comodel_name='res.partner',
            relation='partner_listing',
            column1='category_id',
            column2='partner_id',
            string='Partner Listings',
    )

    excluded_partner_listing_ids = fields.Many2many(
            comodel_name='res.partner',
            relation='excluded_partner_listing',
            column1='category_id',
            column2='partner_id',
            string='Excluded Partner Listings',
    )

    @api.model
    def search(self, args, offset=0, limit=None, order=None, count=False):
        public_group = self.env.ref('base.group_public')
        portal_group = self.env.ref('base.group_portal')
        if public_group in request.env.user.groups_id or portal_group in request.env.user.groups_id:
            cat = request.env.user.partner_id.commercial_partner_id.public_category_ids
            cat += request.env.user.partner_id.public_category_ids
            if cat:
                args += ['|', ('customer_listing', '=', False), ('id', 'in', cat.ids)]
            else:
                args += [('customer_listing', '=', False)]

        return super(ProductPublicCategory, self).search(
            args,
            offset=offset,
            limit=limit,
            order=order,
            count=count,
        )