# -*- coding: utf-8 -*-
from openerp.http import request
from openerp import api, models


class ProductProduct(models.Model):
    _inherit = 'product.product'

    @api.model
    def search(self, args, offset=0, limit=None, order=None, count=False):
        public_group = self.env.ref('base.group_public')
        portal_group = self.env.ref('base.group_portal')
        if public_group in request.env.user.groups_id or portal_group in request.env.user.groups_id:
            public_cat_ids = self.env['product.public.category'].search([('customer_listing', '=', False)]).ids
            listing_cat_ids = request.env.user.partner_id.commercial_partner_id.public_category_ids.ids
            listing_cat_ids += request.env.user.partner_id.public_category_ids.ids
            cat_ids = public_cat_ids + listing_cat_ids
            args += [('public_categ_ids', 'in', cat_ids)]

        return super(ProductProduct, self).search(
                args,
                offset=offset,
                limit=limit,
                order=order,
                count=count,
        )
