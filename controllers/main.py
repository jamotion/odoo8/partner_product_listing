# -*- coding: utf-8 -*-
import logging

from openerp.http import request
from openerp.addons.website_sale_product_characteristics.controllers.main import WebsiteSale

_logger = logging.getLogger(__name__)


class website_sale_listing(WebsiteSale):

    def _get_search_domain(self, search, category, attrib_values):
        domain = super(website_sale_listing, self)._get_search_domain(search, category, attrib_values)

        public_group = request.env.ref('base.group_public')
        portal_group = request.env.ref('base.group_portal')
        if public_group in request.env.user.groups_id or portal_group in request.env.user.groups_id:
            public_cat_ids = request.env['product.public.category'].search([('customer_listing', '=', False)]).ids
            listing_cat_ids = request.env.user.partner_id.commercial_partner_id.public_category_ids.ids
            listing_cat_ids += request.env.user.partner_id.public_category_ids.ids
            cat_ids = public_cat_ids + listing_cat_ids
            domain += [('public_categ_ids', 'in', cat_ids)]

        return domain
